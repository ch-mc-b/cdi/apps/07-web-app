Continous Deployment
====================

Einfache Web Applikation - Erweiterungen für Kubernetes

Die nachfolgenden Befehle sind in einer Kubernetes Umgebung auszuführen.

Docker nach Kubernetes
----------------------

Normalerweise erwartet Kubernetes Deklarative Anweisungen in einer YAML Datei.

Sind diese noch nicht vorhanden, können die YAML Datei Inhalte durch starten der Container als Pods und öffnen der Ports erzeugt werden.

    kubectl create deployment web-app --image registry.gitlab.com/<Kürzel>-cdi/apps/05-web-app:v1.0.0
    kubectl expose deployment/web-app --type "LoadBalancer" --port 8080

Kubernetes arbeitet intern mit den YAML Format. Die erstellen YAML Deklaration können wie folgt ausgegeben werden:

    kubectl get deployment/web-app -o yaml
    kubectl get services/web-app -o yaml

Oder nur die Ausgabe der Ressourcen:    

    kubectl get all

Ausgabe

    NAME                                 READY   STATUS    RESTARTS      AGE
    pod/web-app-559799f76c-6knrg         1/1     Running   0             44s

    NAME                    TYPE           CLUSTER-IP      EXTERNAL-IP   PORT(S)          AGE
    service/web-app         LoadBalancer   10.152.183.34   <pending>     8080:32531/TCP   4s

Der DNS-Name des Kubernetes Cluster und der gemappte Port 8080, hier 32531, geben zusammen den URL. Einfacher geht es mit `curl` und `localhost`:

    curl http://localhost:32531/myapp/myresource


Damit es beim Deployment zu keinen Unterbrüchen kommt, erhöhen wir die Anzahl Instanzen

    kubectl scale --replicas=3 deployment/webapp

Und führen, im laufenden Betrieb, ein **Rolling Update** auf eine neue Version durch:

    kubectl set image deployment/webapp web-app-full-orginal=registry.gitlab.com/<Kürzel>-cdi/apps/05-web-app:v1.0.1

Mittels `kubectl get all` oder dem Dashboard [dev-20-default](https://dev-20-default.mshome.net:8443) können wir den Rolling Update verfolgen.

Nach erfolgreichem Test, müssen die erstellten Ressourcen wieder gelöscht werden. Ansonsten könnte es zu Problemen bei CI/CD kommen.

    kubectl delete deployment/web-app
    kubectl delete service/web-app

Deklaration fürs CI/CD aufbereiten
----------------------------------

Normalerweise würde man einfach die Ausgabe von `kubectl get ... -o yaml` im Repository ablegen.

Wir wollen die YAML Dateien aber für unsere CI/CD Pipeline verwenden.

Deshalb hinterlegen wir den TAG Eintrag dynamisch.

Erzeugt folgende YAML Dateien und legt diese im Git Repository ab:

`deployment.yaml`

    apiVersion: apps/v1
    kind: Deployment
    metadata:
      name: web-app
    spec:
      replicas: 1
      selector:
        matchLabels:
          app: web-app
      template:
        metadata:
          labels:
            app: web-app
        spec:
          containers:
          - name: web-app
            image: ${CI_REGISTRY_IMAGE}:${CI_COMMIT_TAG}
            imagePullPolicy: Always        
            ports:
            - containerPort: 8080
              name: http     

`service.yaml`

    apiVersion: v1
    kind: Service
    metadata:
      labels:
        app: web-app
      name: web-app
    spec:
      ports:
      - port: 8080
        targetPort: 8080
        name: http
      selector:
        app: web-app
      type: LoadBalancer






